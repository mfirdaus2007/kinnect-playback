using UnityEngine;
using System.Collections;
using FileHelpers;

[DelimitedRecord("\t"), IgnoreFirst(1)]
public class HumanPos 
{
	public string gamestate;
	public string stage;
	public string nexttohit;
	
	public float Time;
	
	public float Torsox, Torsoy, Torsoz;
	public float Headx, Heady, Headz;
	public float L_Shoulderx, L_Shouldery, L_Shoulderz;
	public float R_Shoulderx, R_Shouldery, R_Shoulderz;
	public float L_Elbowx, L_Elbowy, L_Elbowz;
	public float R_Elbowx, R_Elbowy, R_Elbowz;
	public float L_Handx, L_Handy, L_Handz;
	public float R_Handx, R_Handy, R_Handz;
	public float L_Hipx, L_Hipy, L_Hipz;
	public float R_Hipx, R_Hipy, R_Hipz;
	public float L_Kneex, L_Kneey, L_Kneez;
	public float R_Kneex, R_Kneey, R_Kneez;
	public float L_Footx, L_Footy, L_Footz; 
	public float R_Footx, R_Footy, R_Footz;
}