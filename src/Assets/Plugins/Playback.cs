﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using FileHelpers;

enum PlaybackState { WAIT, OK, RUNNING, OVER }

public class Playback : MonoBehaviour {

	// the ball man
	Transform Head, Torso;
	Transform L_Shoulder, R_Shoulder;
	Transform L_Elbow, R_Elbow;
	Transform L_Hand, R_Hand;
	Transform L_Hip, R_Hip;
	Transform L_Knee, R_Knee;
	Transform L_Foot, R_Foot;

	HumanPos [] data;

	float fileBeginTime;
	float showBeginTime;
	int currentIndex;
	
	PlaybackState state = PlaybackState.WAIT;

	// 先找到 ball man 的各部位，以方便接下來控制它們的位置。
	void Start () {
				
		Head = GameObject.Find("Head").transform;
		Torso = GameObject.Find("Torso").transform;

		L_Shoulder = GameObject.Find("L_Shoulder").transform;
		R_Shoulder = GameObject.Find("R_Shoulder").transform;
		L_Elbow = GameObject.Find("L_Elbow").transform;
		R_Elbow = GameObject.Find("R_Elbow").transform;
		L_Hand = GameObject.Find("L_Hand").transform;
		R_Hand = GameObject.Find("R_Hand").transform;

		L_Hip = GameObject.Find("L_Hip").transform;
		R_Hip = GameObject.Find("R_Hip").transform;
		L_Knee = GameObject.Find("L_Knee").transform;
		R_Knee = GameObject.Find("R_Knee").transform;
		L_Foot = GameObject.Find("L_Foot").transform;
		R_Foot = GameObject.Find("R_Foot").transform;		
	}	

	// 如果狀態是RUNNING，就逐格更新球人的關節位置。
	void Update () {
		switch(state) {
			case PlaybackState.WAIT:										
				break;

			case PlaybackState.OK:				
				break;

			case PlaybackState.RUNNING:								
				UpdateBallman();	
				break;
			case PlaybackState.OVER:				
				break;
		}			
	}
	
	void OnGUI ()
	{
		Rect rect = new Rect(20,20,100,30);

		switch(state) {
		case PlaybackState.OK:
			if (GUI.Button(rect, "Play")) 
				Play();
			break;

		case PlaybackState.RUNNING:
			//if (GUI.Button(rect, "Play")) 
			//	Play();
			break;

		case PlaybackState.OVER:		
			if (GUI.Button(rect, "Play Again?")){
				state = PlaybackState.OK;
			}
			break;
		}		
	}
	
	// 讀取資料檔，如果一切順利，就將狀態設為OK
	public void LoadFromFile(string filename) {

		if (!File.Exists(filename)) {
			Debug.Log("ERROR: no such file [" + filename + "]!");
			return;
		}
		
		var engine = new FileHelperEngine<HumanPos>();
		data = engine.ReadFile(filename) as HumanPos [];

		Debug.Log("File load success. Number of recored is " + data.Length);
		state = PlaybackState.OK;
	}

	// 只要狀況是OK，就開始播放動畫。
	public void Play() {

		if (state != PlaybackState.OK) {
			Debug.Log("ERROR: not ready to play....");
			return;
		}
		currentIndex = 3;
		fileBeginTime = data[currentIndex].Time;	
		showBeginTime = Time.time;			

		state = PlaybackState.RUNNING;
	}

	Vector3 neg10 = new Vector3(0,0,6);	

	void UpdateBallman() {		
		
		while ((data[currentIndex].Time - fileBeginTime) < (Time.time - showBeginTime)) {

			if (currentIndex >= data.Length -1) {				
				state = PlaybackState.OVER;
				return;
			}

			currentIndex ++;
			HumanPos human = data[currentIndex];

			Head.position = new Vector3(human.Headx, human.Heady, human.Headz);
			Torso.position = new Vector3(human.Torsox, human.Torsoy, human.Torsoz);
			L_Shoulder.position = new Vector3(human.L_Shoulderx, human.L_Shouldery, human.L_Shoulderz);
			R_Shoulder.position = new Vector3(human.R_Shoulderx, human.R_Shouldery, human.R_Shoulderz);
			L_Elbow.position = new Vector3(human.L_Elbowx, human.L_Elbowy, human.L_Elbowz);
			R_Elbow.position = new Vector3(human.R_Elbowx, human.R_Elbowy, human.R_Elbowz);
			L_Hand.position = new Vector3(human.L_Handx, human.L_Handy, human.L_Handz);
			R_Hand.position = new Vector3(human.R_Handx, human.R_Handy, human.R_Handz);
			L_Hip.position = new Vector3(human.L_Hipx, human.L_Hipy, human.L_Hipz);
			R_Hip.position = new Vector3(human.R_Hipx, human.R_Hipy, human.R_Hipz);
			L_Knee.position = new Vector3(human.L_Kneex, human.L_Kneey, human.L_Kneez);
			R_Knee.position = new Vector3(human.R_Kneex, human.R_Kneey, human.R_Kneez);
			L_Foot.position = new Vector3(human.L_Footx, human.L_Footy, human.L_Footz);
			R_Foot.position = new Vector3(human.R_Footx, human.R_Footy, human.R_Footz);

			L_Shoulder.LookAt(L_Elbow.position);
			R_Shoulder.LookAt(R_Elbow.position);
			L_Elbow.LookAt(L_Hand.position);
			R_Elbow.LookAt(R_Hand.position);
			L_Hip.LookAt(L_Knee.position);
			R_Hip.LookAt(R_Knee.position);
			
			Camera.main.transform.position = Torso.position + neg10;
		}
	}
}

